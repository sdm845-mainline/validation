# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <joelselvaraj.oss@gmail.com>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=6.11.0_rc2
pkgrel=0
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/linux"
license="GPL-2.0-only"
options="!check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community"
makedepends="bash bison findutils flex installkernel openssl-dev perl zstd python3"

_config="config-$_flavor.$arch"
_tag="sdm845-6.11.0_rc2"

# Source
source="
	linux-$_tag.tar.gz::https://gitlab.com/sdm845-mainline/linux/-/archive/$_tag/linux-$_tag.tar.gz
	$_config
"
builddir="$srcdir/linux-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	if [ -e "$builddir/arch/$_carch/boot/vmlinuz.efi" ]; then
		# ZBOOT EFI decompressor for EFI booting
		install -Dm644 "$builddir/arch/$_carch/boot/vmlinuz.efi" \
			"$pkgdir/boot/linux.efi"

		# Old GZIP'd kernel image for boot.img compatibility
		install -Dm644 "$builddir/arch/$_carch/boot/vmlinuz" \
			"$pkgdir/boot/vmlinuz"
	else
		echo "WARNING: CONFIG_ZBOOT not enabled!"
		install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
			"$pkgdir/boot/vmlinuz"
	fi

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs-tmp

	mkdir -p "$pkgdir"/boot/dtbs/qcom
	cp -r "$pkgdir"/boot/dtbs-tmp/qcom/sdm8* "$pkgdir"/boot/dtbs/qcom
	rm -rf "$pkgdir"/boot/dtbs-tmp

	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
8f51280445a960102f53ba5fb2ff3a80178ed89d00c1ea8e9fffedcf1664ce150218a82ddca6d3015223ec0f404fa6575e8725cbf8abe1b1a636d67ca77a71e7  linux-sdm845-6.11.0_rc2.tar.gz
0faf26d3b780f7b9ec2c06820ab8ad361dd583e81c0f9c507a4300c209524869ff1dc4b083e29f9dc9e945030031ccb437a5283ded4ce31158fb842128f4c165  config-postmarketos-qcom-sdm845.aarch64
"

